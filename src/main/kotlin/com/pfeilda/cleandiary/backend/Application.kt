package com.pfeilda.cleandiary.backend
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

fun Application.module() {
    routing {
        systemRoutes()
    }
}

fun Route.systemRoutes() {
    get ("/ping") {
        call.respondText(
            "{\"ping\":\"pong\"}",
            ContentType.Application.Json.withCharset(Charsets.UTF_8),
            HttpStatusCode.OK
        )
    }
    get ("/health") {
        call.respondText(
            "{" +
                    "\"status\":{" +
                        "\"backend\":\"available\"" +
                    "}" +
                "}",
            ContentType.Application.Json.withCharset(Charsets.UTF_8),
            HttpStatusCode.OK
        )
    }
}
