package integration.endpoint

import com.pfeilda.cleandiary.backend.module
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.server.testing.*
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Test

class SystemEndpointTest {
    @Test
    fun ping() = withTestApplication(Application::module){
        with(handleRequest(HttpMethod.Get, "/ping")) {
            GeneralTests().executeOn(response)
            assertThat("{\"ping\":\"pong\"}", `is`(equalTo(response.content)))
        }
    }

    @Test
    fun health() = withTestApplication(Application::module){
        with(handleRequest(HttpMethod.Get, "/health")) {
            GeneralTests().executeOn(response)
            assertThat(
                "{" +
                    "\"status\":{" +
                        "\"backend\":\"available\"" +
                    "}" +
                "}",
                `is`(equalTo(response.content))
            )
        }
    }
}