package integration.endpoint

import io.ktor.http.*
import io.ktor.server.testing.*
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert

class GeneralTests {
    fun executeOn(response: TestApplicationResponse){
        MatcherAssert.assertThat(HttpStatusCode.OK, CoreMatchers.`is`(CoreMatchers.equalTo(response.status())))
        MatcherAssert.assertThat(
            ContentType.Application.Json,
            CoreMatchers.`is`(CoreMatchers.equalTo(response.contentType().withoutParameters()))
        )
    }
}
