import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val ktor_version: String = "1.4.0"

plugins {
    kotlin("jvm") version "1.4.30"
    application
}

group = "com.cleandiary."
version = "1.0-SNAPSHOT"

repositories {
    jcenter()
    mavenCentral()
    maven { url = uri("https://dl.bintray.com/kotlin/kotlinx") }
    maven { url = uri("https://dl.bintray.com/kotlin/ktor") }
}

dependencies {
    testImplementation(kotlin("test-junit5"))
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.6.0")
    testImplementation("org.hamcrest:hamcrest-all:1.3")
    testImplementation("io.ktor:ktor-server-test-host:$ktor_version")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.6.0")

    implementation("io.ktor:ktor-server-netty:$ktor_version")
    implementation("io.ktor:ktor-html-builder:$ktor_version")
}

tasks.test {
    useJUnitPlatform()
}

task("stage") {
    dependsOn("build", "jar")
}

tasks.withType<KotlinCompile>() {
    kotlinOptions.jvmTarget = "13"
}

application {
    mainClassName = "com.pfeilda.cleandiary.backend.ApplicationKt"
}